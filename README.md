# Snakemake workflow: Isoseq3.3

[![Snakemake](https://img.shields.io/badge/snakemake-≥5.19.3-brightgreen.svg)](https://snakemake.bitbucket.io)

This workflow implements the PacBio Isoseq v3.3.0 pipeline.

**Note:** Currently this workflow is limited to non-multiplexed samples.

## Authors

* Terry Bertozzi

## Usage

If you use this workflow in a paper, don't forget to cite the URL of this (original) repository and, if available, its DOI (see above).

### Step 1: Obtain a copy of this workflow

1. Create a new github repository using this workflow [as a template](https://help.github.com/en/articles/creating-a-repository-from-a-template).
2. [Clone](https://help.github.com/en/articles/cloning-a-repository) the newly created repository to your local system, into the place where you want to perform the data analysis.

### Step 2: Configure workflow

#### General settings

Configure the workflow according to your needs by editing the `config.yaml` file in the `config/` folder.

#### Sample sheet

* Add samples to the tab delimited file `config/samples.tsv`.
* For each sample, add one or more sequencing units (isoseq flow cells) and the paths to the BAM files. Data should be placed into the `data` folder.
*Note:* An example `samples.tsv` is provided which references test data in the `data` directory.

#### Primer file

The workflow requires a fasta formatted primer file for primer removal. Primers must be named according to [this document](https://github.com/pacificbiosciences/barcoding/#how-can-i-demultiplex-isoseq-data). The primer file should be placed into the `resources/` folder.

### Step 3: Install singularity

This workflow uses singularity containers to run the various parts of the Isoseq3 pipeline and maintain version control. If singularity is not installed on your system, you can install it by following the instructions [here](https://sylabs.io/singularity/)

### Step 4: Install Snakemake

Install Snakemake using [conda](https://conda.io/projects/conda/en/latest/user-guide/install/index.html):

    conda create -c bioconda -c conda-forge -n snakemake snakemake

For installation details, see the [instructions in the Snakemake documentation](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html).

### Step 5: Execute workflow
The workflow must be run from the top directory

     cd isoseq3.3

Activate the conda environment:

    conda activate snakemake

Test your configuration by performing a dry-run via

    snakemake --use-singularity -n

Execute the workflow locally using `$N` cores via

    snakemake --use-singularity --cores $N

See the [Snakemake documentation](https://snakemake.readthedocs.io/en/stable/executable.html) for further details about running the workflow on a cluster.

### Step 6: Investigate results

After successful execution, you can create a self-contained interactive HTML report with all results via:

    snakemake --report report.html

An example (using some trivial test data) can be seen [here](https://cdn.rawgit.com/snakemake-workflows/rna-seq-kallisto-sleuth/master/.test/report.html).
