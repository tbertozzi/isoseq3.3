def get_bam(wildcards):
    return data.loc[(wildcards.sample, wildcards.unit), ["file"]]


rule make_ccs:
    input:
        get_bam
    output:
        consensus = "results/ccs/{sample}-{unit}.ccs.bam",
        report = "results/reports/{sample}-{unit}_ccs_report.txt"
    log:
        "results/logs/ccs.{sample}-{unit}.log"
    params:
        minrq="0.95"
    container:
        "docker://quay.io/biocontainers/pbccs:4.2.0--1"
    shell:
        "ccs --min-passes 1 --min-rq {params.minrq} --report-file {output.report} --log-file {log} {input} {output.consensus}"
