rule merge:
    input:
        lambda wildcards: expand("results/flnc/{sample}-{unit}.flnc.bam", sample=wildcards.sample, unit= samples[wildcards.sample])
    output:
        "results/flnc/{sample}.flnc.fofn"
    log:
        "../logs/merge.{sample}.log"
    params:
        pattern = "{sample}-*.flnc.bam"
    shell:
        """
        (cd results/flnc && ls {params.pattern}) > {output}
        > {log} 2>&1
        cd ../..
        """
