rule cluster:
    input:
        "results/flnc/{sample}.flnc.fofn"
    output:
        "results/cluster/{sample}.clustered.bam"
    log:
        "results/logs/cluster.{sample}.log"
    container:
        "docker://quay.io/biocontainers/isoseq3:3.3.0--0"
    shell:
        "isoseq3 cluster --use-qvs --verbose --log-file {log} {input} {output}"
