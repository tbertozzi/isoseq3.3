rule refine:
    input:
        clean = expand("results/lima/{{sample}}-{{unit}}.{string}", sample = samples, unit = units.keys(), string =  lima_str),
        primers = config["primers"]
    output:
        "results/flnc/{sample}-{unit}.flnc.bam"
    log:
        "results/logs/refine.{sample}-{unit}.log"
    container:
        "docker://quay.io/biocontainers/isoseq3:3.3.0--0"
    shell:
        "isoseq3 refine --require-polya --log-file {log} {input.clean} {input.primers} {output}"
