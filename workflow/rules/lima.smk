def cat_str(file):
    fasta=open(file)
    head=[]

    for line in fasta:
        if line[0] == '>':
            line=line.strip()
            head.append(line[1:len(line)])
    return "--".join(head)+".bam"

lima_str = cat_str(config["primers"])

rule lima:
    input:
        consensus = "results/ccs/{sample}-{unit}.ccs.bam",
        primers = config["primers"]
    output:
        expand("results/lima/{{sample}}-{{unit}}.{string}", sample = samples, unit = units.keys(), string =  lima_str)
    log:
        "results/logs/lima.{sample}-{unit}.log"
    params:
        prefix = "results/lima/{sample}-{unit}.bam"
    container:
        "docker://quay.io/biocontainers/lima:1.11.0--0"
    shell:
        "lima --isoseq --log-file {log} {input.consensus} {input.primers} {params.prefix}"
