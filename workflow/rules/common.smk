from snakemake.utils import validate, min_version
import pandas as pd

##--- set minimum snakemake version ---##
min_version("5.19.3")

##--- load and validate config and sample sheets ---##
configfile: "config/config.yaml"
validate(config, schema="../schemas/config.schema.yaml")

data = pd.read_csv(config["samples"], dtype=str, sep="\t").set_index(["sample", "unit"], drop=False)
data.index = data.index.set_levels([i.astype(str) for i in data.index.levels])  # enforce str in index
validate(data, schema="../schemas/samples.schema.yaml")


samples = data.groupby(level='sample')['unit'].apply(list).to_dict()
units = data.set_index("unit").to_dict()["file"]

#samples = data.index.get_level_values('sample').unique().tolist()
#units = data.index.get_level_values('unit').unique().tolist

##--- setup report ---##
report: "../../results/reports/workflow.rst"
